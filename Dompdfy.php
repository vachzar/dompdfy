<?php
defined('BASEPATH') or exit('No direct script access allowed');/**
 * Dompfy
 * This is a Dompdf wrapper class/library for Codeigniter
 *
 */

// uncomment  code below if you use Download and Install method
// require_once APPPATH .'/third_party/dompdf/autoload.inc.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;



class Dompdfy
{

    protected $dompdf;
    public function __construct()
    {
        // instantiate and use the dompdf class
        $this->dompdf = new Dompdf();
    }

    public function loadHtml($html)
    {
        $this->dompdf->loadHtml($html);
        $this->dompdf->set_option('isRemoteEnabled', TRUE);
        return $this;

    }

    public function setPaper($size, $orientation = "portrait"){
        $this->dompdf->setPaper($size, $orientation);
        return $this;
    }

    public function set_option($option, $value){
        $this->dompdf->set_option($option, $value);
        return $this;
    }

    public function stream($filename = 'document.pdf') // Output the generated PDF to Browser
    {
    	$this->dompdf->render();
        $this->dompdf->stream($filename);
        return $this;
    }
}
