# Dompdfy

a Dompdf Wrapper Class for Codeigniter 3.x +

## Easy Installation

Copy `Dompdfy.php` to your `application/libraries`.

There are 2 way to use Dompdf. 

### Install with composer

To install with [Composer](https://getcomposer.org/), simply require the
latest version of this package.

```bash
composer require dompdf/dompdf
```

Make sure that you have set `application/config/config.php` to enable Composer autoload. 
```php
$config['composer_autoload'] = 'vendor/autoload.php';
```

### Download and install

Download an archive of dompdf and extract it into the directory `application\third_party\`
 * You can download stable copies of dompdf from
   https://github.com/dompdf/dompdf/releases
 * Or download a nightly (the latest, unreleased code) from
   http://eclecticgeek.com/dompdf

Uncomment `require_once` in library to load dompdf, libraries,
and helper functions in your Codeigniter:

```php
// uncomment code below if you use Download and Install method
require_once APPPATH .'/third_party/dompdf/autoload.inc.php';
```

## Quick Start

Just pass your HTML in to dompdf and stream the output:

```php
// load library
$this->load->library('dompdfy');

$this->dompdfy->loadHtml('hello world');

// (Optional) Setup the paper size and orientation
$this->dompdfy->setPaper('A4', 'landscape');

// Render the HTML as PDF and output the generated PDF to Browser
$this->dompdfy->stream();
```
### Setting Options

Set options at run time

```php
$dompdf->set_option('defaultFont', 'Courier');
```

See [Dompdf\Options](https://github.com/dompdf/dompdf/blob/master/src/Options.php) for a list of available options.

## Limitations (Known Issues)

 * For simplicity, setting up options during dompdf instantiation not yet available. 
